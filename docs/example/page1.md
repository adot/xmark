# Page 1


# h1

content

*italic* **bold** **_both_**

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
at quam sem. Praesent **venenatis odio sed enim lacinia vehicula. Pellentesque
quam leo, cursus varius** auctor vitae, ullamcorper sit amet justo. Cras eget
pretium metus. In rhoncus **_felis et velit suscipit dapibus_**. Duis et enim nec
mi convallis dapibus. Sed id aliquet massa. Morbi interdum neque sit amet
mauris volutpat, vitae *interdum mauris fringilla*. Donec sagittis ornare elit
eget elementum.  

## h2

>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
>  at quam sem. Praesent **venenatis odio sed enim lacinia vehicula. Pellentesque
>  quam leo, cursus varius** auctor vitae, ullamcorper sit amet justo. Cras eget
>  pretium metus. In rhoncus **_felis et velit suscipit dapibus_**. Duis et enim nec
>  mi convallis dapibus. Sed id aliquet massa. Morbi interdum neque sit amet
>  mauris volutpat, vitae *interdum mauris fringilla*. Donec sagittis ornare elit
>  eget elementum.  

### h3

<!-- TODO: Use full-width for code -->
```Rust
/// The parsed `SUMMARY.md`, specifying how the book should be laid out.
#[derive(Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Summary {
    /// An optional title for the `SUMMARY.md`, currently just ignored.
    pub title: Option<String>,
    /// Chapters before the main text (e.g. an introduction).
    pub prefix_chapters: Vec<SummaryItem>,
    /// The main numbered chapters of the book, broken into one or more possibly named parts.
    pub numbered_chapters: Vec<SummaryItem>,
    /// Items which come after the main document (e.g. a conclusion).
    pub suffix_chapters: Vec<SummaryItem>,
}
```

#### h4

'so-called' "quotes"

##### h5


*`heeel`*  **`dfsa`** **_`fsddfdfgsdfn`_**

###### h6



Text can be **bold**, _italic_, or ~~strikethrough~~.

[Link to another page](./another-page.html).

There should be whitespace between paragraphs.

There should be whitespace between paragraphs. We recommend including a README, or a file with information about your project.

# Header 1

This is a normal paragraph following a header. GitHub is a code hosting platform for version control and collaboration. It lets you and others work together on projects from anywhere.

## Header 2

> This is a blockquote following a header.
>
> When something is important enough, you do it even if the odds are not in your favor.

### Header 3

```JavaScript
// Javascript code with syntax highlighting.
var fun = function lang(l) {
  dateformat.i18n = require('./lang/' + l)
  return true;
}
```

```Ruby
# Ruby code with syntax highlighting
GitHubPages::Dependencies.gems.each do |gem, version|
  s.add_dependency(gem, "= #{version}")
end
```

#### Header 4

*   This is an unordered list following a header.
*   This is an unordered list following a header.
*   This is an unordered list following a header.

##### Header 5

1.  This is an ordered list following a header.
2.  This is an ordered list following a header.
3.  This is an ordered list following a header.

###### Header 6

| head1        | head two          | three |
|:-------------|:------------------|:------|
| ok           | good swedish fish | nice  |
| out of stock | good and plenty   | nice  |
| ok           | good `oreos`      | hmm   |
| ok           | good `zoute` drop | yumm  |

### There's a horizontal rule below this.

* * *

### Here is an unordered list:

*   Item foo
*   Item bar
*   Item baz
*   Item zip

### And an ordered list:

1.  Item one
1.  Item two
1.  Item three
1.  Item four

### And a nested list:

- level 1 item
  - level 2 item
  - level 2 item
    - level 3 item
    - level 3 item
- level 1 item
  - level 2 item
  - level 2 item
  - level 2 item
- level 1 item
  - level 2 item
  - level 2 item
- level 1 item

### Small image

![Octocat](https://github.githubassets.com/images/icons/emoji/octocat.png)

### Large image

![Branching](https://guides.github.com/activities/hello-world/branching.png)


### Definition lists can be used with HTML syntax.

<dl>
<dt>Name</dt>
<dd>Godzilla</dd>
<dt>Born</dt>
<dd>1952</dd>
<dt>Birthplace</dt>
<dd>Japan</dd>
<dt>Color</dt>
<dd>Green</dd>
</dl>

```
Long, single-line code blocks should not wrap. They should horizontally scroll if they are too long. This line should be long enough to demonstrate this.
```

```
The final element.
```