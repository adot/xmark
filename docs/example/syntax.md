# Syntax highlighting

## Plain text

```text
Enumerating objects: 15, done.
Counting objects: 100% (15/15), done.
Delta compression using up to 8 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (8/8), 6.36 KiB | 3.18 MiB/s, done.
Total 8 (delta 6), reused 0 (delta 0), pack-reused 0
remote: . Processing 1 references
remote: Processed 1 references in total
To codeberg.org:adot/xmark.git
   45fffd9..b2ef77d  trunk -> trunk
```

## Rust

```rust
pub struct HeadingLinks<'a, 'b, I> {
    inner: I,
    toc: Option<&'b mut TocBuilder>,
    buf: VecDeque<SpannedEvent<'a>>,
    id_map: IdMap,
    title: &'b mut Option<String>,
}

impl<'a, 'b, I> HeadingLinks<'a, 'b, I> {
    pub fn new(iter: I, toc: Option<&'b mut TocBuilder>, title: &'b mut Option<String>) -> Self {
        HeadingLinks {
            inner: iter,
            toc,
            title,
            buf: VecDeque::new(),
            id_map: Default::default(),
        }
    }
}
```

## Shell
```bash
if test -z "$BASH_VERSION$ZSH_VERSION" \
    && (test "X`print -r -- $as_echo`" = "X$as_echo") 2>/dev/null; then
  as_echo='print -r --'
  as_echo_n='print -rn --'
elif (test "X`printf %s $as_echo`" = "X$as_echo") 2>/dev/null; then
  as_echo='printf %s\n'
  as_echo_n='printf %s'
else
  if test "X`(/usr/ucb/echo -n -n $as_echo) 2>/dev/null`" = "X-n $as_echo"; then
    as_echo_body='eval /usr/ucb/echo -n "$1$as_nl"'
    as_echo_n='/usr/ucb/echo -n'
  else
    as_echo_body='eval expr "X$1" : "X\\(.*\\)"'
    as_echo_n_body='eval
      arg=$1;
      case $arg in #(
      *"$as_nl"*)
	expr "X$arg" : "X\\(.*\\)$as_nl";
	arg=`expr "X$arg" : ".*$as_nl\\(.*\\)"`;;
      esac;
      expr "X$arg" : "X\\(.*\\)" | tr -d "$as_nl"
    '
    export as_echo_n_body
    as_echo_n='sh -c $as_echo_n_body as_echo'
  fi
  export as_echo_body
  as_echo='sh -c $as_echo_body as_echo'
fi
```

## C

```c
/*
** Check to see if the i-th bit is set.  Return true or false.
** If p is NULL (if the bitmap has not been created) or if
** i is out of range, then return false.
*/
int sqlite3BitvecTestNotNull(Bitvec *p, u32 i){
  assert( p!=0 );
  i--;
  if( i>=p->iSize ) return 0;
  while( p->iDivisor ){
    u32 bin = i/p->iDivisor;
    i = i%p->iDivisor;
    p = p->u.apSub[bin];
    if (!p) {
      return 0;
    }
  }
  if( p->iSize<=BITVEC_NBIT ){
    return (p->u.aBitmap[i/BITVEC_SZELEM] & (1<<(i&(BITVEC_SZELEM-1))))!=0;
  } else{
    u32 h = BITVEC_HASH(i++);
    while( p->u.aHash[h] ){
      if( p->u.aHash[h]==i ) return 1;
      h = (h+1) % BITVEC_NINT;
    }
    return 0;
  }
}
```

## C++

```cpp
class FDWatcher
{
public:
    using Callback = std::function<void (FDWatcher& watcher, FdEvents events, EventMode mode)>;
    FDWatcher(int fd, FdEvents events, Callback callback);
    FDWatcher(const FDWatcher&) = delete;
    FDWatcher& operator=(const FDWatcher&) = delete;
    ~FDWatcher();

    int fd() const { return m_fd; }
    FdEvents events() const { return m_events; }
    FdEvents& events() { return m_events; }

    void run(FdEvents events, EventMode mode);

    void close_fd();
    void disable() { m_fd = -1; }

private:
    int      m_fd;
    FdEvents m_events;
    Callback m_callback;
};
```

## JSON

```json
{
  "title": "Summary",
  "prefix_chapters": [],
  "numbered_chapters": [
    {
      "Link": {
        "name": "mdBook",
        "location": "README.md",
        "number": [
          1
        ],
        "nested_items": []
      }
    },
    {
      "Link": {
        "name": "Command Line Tool",
        "location": "cli/README.md",
        "number": [
          2
        ],
        "nested_items": [
          {
            "Link": {
              "name": "serve",
              "location": "cli/serve.md",
              "number": [
                2,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "test",
              "location": "cli/test.md",
              "number": [
                2,
                5
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "clean",
              "location": "cli/clean.md",
              "number": [
                2,
                6
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Continuous Integration",
        "location": "continuous-integration.md",
        "number": [
          4
        ],
        "nested_items": []
      }
    },
    "Separator"
  ],
  "suffix_chapters": [
    {
      "Link": {
        "name": "Contributors",
        "location": "misc/contributors.md",
        "number": null,
        "nested_items": []
      }
    }
  ]
}
```

## Javascript

```js
let rtoc_items = [].slice.call(rtoc.querySelectorAll("li").map((item) => {
  let anchor = item.querySelector("a");
  let target = document.getElementById(anchor.getAttribut("href").slice(1));
  return { anchor, target };
});
```

## Ruby

```Ruby
# Ruby code with syntax highlighting
GitHubPages::Dependencies.gems.each do |gem, version|
  s.add_dependency(gem, "= #{version}")
end
```

## HTML

```html
<ol>
  <li>
    <a href="#plain-text"><b>1</b> Plain text</a>
  </li>
  <li>
    <a href="#rust"><b>2</b> Rust</a>
  </li>
  <li>
    <a href="#shell"><b>3</b> Shell</a>
  </li>
  <li>
    <a href="#c"><b>4</b> C</a>
  </li>
  <li>
    <a href="#c-1"><b>5</b> C++</a>
  </li>
  <li>
    <a href="#json"><b>6</b> JSON</a>
  </li>
  <li>
    <a href="#javascript" class="active"><b>7</b> Javascript</a>
  </li>
</ol>
```

## TOML

```toml
[package]
name = "xmark"
version = "0.1.0"
authors = ["Nixon Enraght-Moony <nixon.emoony@gmail.com>"]
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
# TODO: Strip out all the non theme stuff, or convince them to spin off
# the sublime-themes into a seperate crate.
bat = { git = "https://github.com/xmark-rs/bat" }
bincode = "1.3.1"
clap = "3.0.0-beta.2"
```

## Python

```py
# We must staticly know all the @inports of the sass
def invoke(ninja, sass, css, imports=[], pritty=None):
    if pritty is None:
        pritty = sass

    ninja.build(
        css,
        RULE_SASS,
        imports + [sass],
        variables={VAR_NAME: pritty, VAR_PRIMARY: sass},
    )
```

## Markdown

```md
## heading

*fds* `sdaf` [d](dsfa.co)

- a
- b
```

## Makefile
```make
bench: gen
	hyperfine "ninja -C corpus/_build -t clean && ninja -C corpus/_build"

doc: 
	cargo doc --document-private-items

.PHONY: test fmt clean gen build clean-hard lint rust-build doc
```

## Asm

```asm
func:
    push   ebp
    mov    ebp,esp
    sub    esp,0x10
    mov    eax,DWORD PTR [ebp+0xc]
    mov    DWORD PTR [ebp-0x4],eax
    mov    eax,DWORD PTR [ebp+0x8]
    mov    DWORD PTR [ebp-0x8],eax
    jmp    0x50c <asm2+31>
    add    DWORD PTR [ebp-0x4],0x1
    add    DWORD PTR [ebp-0x8],0xa9
    cmp    DWORD PTR [ebp-0x8],0x47a6
    jle    0x501 <asm2+20>
    mov    eax,DWORD PTR [ebp-0x4]
    leave  
    ret
```