# xmark

## Quickstart

```
git clone https://codeberg.org/adot/xmark.git --recurse-submodules
cd xmark
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
cargo build
make gen
make build
```

## Requirements

- Cargo (latest stable)
- Python 3.9
- Ninja
- GNU Make
- Recent Browser