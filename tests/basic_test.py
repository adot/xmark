from os.path import join as j

import utils

BOOK_NAME = "basic"
BASIC_BOOK = j(utils.BOOKS_DIR, BOOK_NAME)


def test_builds_sucessfully(tmp_path):
    _, build_dir = utils.cp_and_build(BASIC_BOOK, tmp_path)

    html_dir = j(build_dir, "html", BOOK_NAME)

    for name in ["page1.html", "page2.html", "page3.html"]:
        utils.check_exists(j(html_dir, name))


def test_edit(tmp_path):
    book, build_dir = utils.cp_and_build(BASIC_BOOK, tmp_path)
    with open(j(book, "page2.md"), "a") as f:
        f.write("More")

    r = utils.run_ninja(build_dir, True)
    assert "[1/1] Building ./page2.md" in r.stdout.decode()


def test_new_page(tmp_path):
    book, build_dir = utils.cp_and_build(BASIC_BOOK, tmp_path)
    with open(j(book, "SUMMARY.md"), "a") as f:
        f.write("\n[page4](./page4.md)")
    with open(j(book, "page4.md"), "w") as f:
        f.write("page4")
    r = utils.run_ninja(build_dir, True)
    stdout = r.stdout.decode()
    assert "[0/1] Regererating build files." in stdout
    assert "Starting xmark ninjen" in stdout
    assert "/4] Building ./page4.md" in stdout
    assert "/4] Building ./page1.md" in stdout


# def test_b
