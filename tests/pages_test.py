import re

from os.path import join as j
from lxml import etree

import utils

DDOCK_BOCK = j(utils.BOOKS_DIR, "pages")

# TODO: Figure out how to have every logical test be it's
# own function without rebuilding the book for each test
def test_pages(tmp_path):
    html_parser = etree.HTMLParser(recover=False)

    _, build = utils.cp_and_build(DDOCK_BOCK, tmp_path)
    html = j(build, "html", "pages")

    # TODO: Source this from somwehrere sensible
    # TODO: Dank blessing framework
    # TODO: Be somewhat more sensable than this
    # TODO: Extract core logic from data
    # TODO: Try cssselect <https://pypi.org/project/cssselect/> instead of xpath
    for page, tests in [
        (
            "basic.html",
            [
                (
                    '//div[@class="content"]/h1',
                    '<h1 id="basic"><a href="#basic" class="autolink">Basic</a></h1>',
                ),
                ('//div[contains(@class, "smalltoc")]/h1', "<h1>Basic</h1>"),
                ('//div[@class="content"]/p[1]', "<p><em>bold</em></p>"),
                ('//div[@class="content"]/p[2]', "<p><strong>italic</strong></p>"),
                (
                    '//div[@class="content"]/p[3]',
                    "<p><strong><em>both</em></strong></p>",
                ),
                (
                    '//div[@class="content"]/p[4]',
                    "<p><em><strong>both</strong></em></p>",
                ),
            ],
        )
    ]:
        with open(j(html, page)) as f:
            tree = etree.parse(f, html_parser)

        for path, text in tests:
            node = tree.xpath(path)
            [node] = node

            el_text = clean_text(etree.tostring(node))

            assert el_text == text


def clean_text(s):
    if not isinstance(s, str):
        s = s.decode()

    return re.sub(r"[ \n\t]{1,}", " ", s.strip())
