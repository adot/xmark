import sys
import os
import subprocess
import shutil
from os.path import join as j

NINJEN_FILE = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), "ninjen", "ninjen.py"
)

PY_EXE = sys.executable

# TODO: Less of a bodge
for i in ["/usr/bin/ninja", "/usr/local/bin/ninja"]:
    if os.path.exists(i):
        NINJA_EXE = i
        break


BOOKS_DIR = os.path.join(os.path.dirname(__file__), "books")


def check_sucess(errno, should, output=""):
    if should is not None:
        if should:
            print(output)
            assert errno == 0
        else:
            assert errno != 1


def check_exists(p):
    assert os.path.exists(p)


def cp_and_build(from_, to):
    book = shutil.copytree(from_, to, dirs_exist_ok=True)
    run_nj(book, True)
    build_dir = j(book, "_build")
    run_ninja(build_dir, True)
    return (book, build_dir)


def run_nj(dir, should_suceed=None):
    # assert False, PY_EXE

    resp = subprocess.run([PY_EXE, NINJEN_FILE, dir], capture_output=True)
    check_sucess(resp.returncode, should_suceed, str(resp.stderr))
    return resp


def run_ninja(dir, should_suceed=None):
    resp = subprocess.run([NINJA_EXE, "-C", dir], capture_output=True)
    check_sucess(resp.returncode, should_suceed)
    return resp


# https://stackoverflow.com/a/28154841

# def generate(dir):
#     ninjen.Build(dir)
