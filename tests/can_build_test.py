import os
from os.path import join as j

import utils

ROOT_DIR = os.path.dirname(os.path.dirname(__file__))


def test_build_corpus(tmp_path):
    utils.cp_and_build(j(ROOT_DIR, "corpus"), tmp_path)


def test_build_docs(tmp_path):
    utils.cp_and_build(j(ROOT_DIR, "docs"), tmp_path)
