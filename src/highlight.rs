use pulldown_cmark::{CodeBlockKind, CowStr, Event, Tag};

use eyre::{eyre, Result};
use syntect::html::ClassedHTMLGenerator;
use syntect::parsing::SyntaxSet;
use syntect::util::LinesWithEndings;

use crate::cmd::css::CLASS_STYLE;

pub struct Hightlighter<I> {
    iter: I,
    langs: Option<SyntaxSet>,
}

impl<I> Hightlighter<I> {
    pub fn new(iter: I) -> Self {
        let langs = None;

        Self { iter, langs }
    }

    // Load the syntax set.
    fn load_langs(&mut self) -> &SyntaxSet {
        //  This is weird, but nicer things are blocked on polonious

        if let Some(ref l) = self.langs {
            return l;
        }
        // TODO: This is ~10x more expensive than everything else
        // We literaly spend all are time deserializing from memory
        // We sould split up SyntaxSet to one per lang, and load when
        // The lang is needed.
        // But the SyntaxSet API is not fun
        // TODO: Figure out what newlines means
        // TODO: Figure out how much worse bat is lol
        let syns = bat::assets::HighlightingAssets::get_integrated_syntaxset();

        self.langs = Some(syns);

        self.load_langs()
    }

    fn lang_name(&self, l: &str) -> Option<&'static str> {
        if l.is_empty() {
            return None;
        }

        let l_lower = l.split(',').next().unwrap().trim().to_lowercase();
        if l_lower == "text" {
            return None;
        }
        let cannon_lang = match l_lower.as_ref() {
            "text" | "txt" | "ignore" => {
                return None;
            }
            // TODO: Make this a conf setting for the
            // book to redirect mir to rust
            "rs" | "rust" | "mir" | "rust2018" => "Rust",
            // "console" => "Shell-Unix-Generic",
            "shell" | "console" | "bash" | "sh" => "Shell-Unix-Generic",
            "c" => "C",
            "cpp" | "c++" => "C++",
            "diff" => "Diff",
            "yaml" => "YAML",
            // TODO: Otherwise we panic deep in syntech on corpus
            // I think this is commenrs, but lol
            "json" => "JavaScript",
            "javascript" | "js" => "JavaScript",
            "ruby" => "Ruby",
            // TODO: Find a hbs theme and submit to bat
            "html" | "hbs" | "handlebars" => "HTML",
            "toml" => "TOML",
            "python" | "py" => "Python",
            "markdown" | "md" => "Markdown",
            "makefile" | "make" => "Makefile",
            "powershell" | "cmd" => "PowerShell",
            "asm" => "Assembly (x86_64)",
            unknown => {
                // TODO: Diagrams
                eprintln!("WARNING: Unknown language {}", unknown);
                return None;
            }
        };
        Some(cannon_lang)
    }

    fn hightlight(&mut self, lang: &str, code: &str) -> String {
        // TODO: Handle non lang items. eg diagrams, plugins, etc
        // TODO: Handle case, abreviations
        // eg js, javascript => JavaScript
        let lang_name = self.lang_name(lang);
        let langs = self.load_langs();

        let lang = match lang_name {
            Some(l) => langs.find_syntax_by_name(l).unwrap(),
            None => {
                // TODO: Be clever with allocs
                return code.to_owned();
            }
        };

        let mut html_generator =
            ClassedHTMLGenerator::new_with_class_style(lang, &langs, CLASS_STYLE);
        for line in LinesWithEndings::from(code) {
            html_generator.parse_html_for_line_which_includes_newline(line);
        }

        html_generator.finalize()
    }
}

impl<'a, I> Iterator for Hightlighter<I>
where
    I: Iterator<Item = Event<'a>>,
{
    type Item = Result<Event<'a>>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.next() {
            Some(Event::Start(Tag::CodeBlock(CodeBlockKind::Fenced(lang)))) => {
                if lang.as_ref() == "" {
                    return Some(Ok(Event::Start(Tag::CodeBlock(CodeBlockKind::Fenced(
                        lang,
                    )))));
                }

                let mut code = String::new();

                loop {
                    match self.iter.next() {
                        Some(Event::Text(t)) => code.push_str(t.as_ref()),
                        Some(Event::End(Tag::CodeBlock(CodeBlockKind::Fenced(_)))) => {
                            break;
                        }
                        e => {
                            return Some(Err(eyre!(
                                "Expeced end of code block, found {:?} around `{}`",
                                e,
                                code
                            )))
                        }
                    }
                }

                let s = self.hightlight(lang.as_ref(), code.as_ref());

                Some(Ok(Event::Html(CowStr::Boxed(
                    format!("<pre><code>{}</code></pre>", s).into_boxed_str(),
                ))))
            }
            e => Ok(e).transpose(),
        }
    }
}
