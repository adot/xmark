use clap::Clap;
use eyre::Result;

mod cmd;
mod highlight;
mod ltoc;
mod mdutil;
mod summary;

#[derive(Clap)]
struct Opts {
    #[clap(subcommand)]
    sub: Subcommand,
}
#[derive(Clap)]
enum Subcommand {
    Internal(InternalW),
    Version,
}
#[derive(Clap)]
struct InternalW {
    #[clap(subcommand)]
    sub: Internal,
}

#[derive(Clap)]
enum Internal {
    ParseSummary(cmd::parse_summary::Cmd),
    Page(cmd::page::Cmd),
    ParseTemplate(cmd::parse_template::Cmd),
    ParseSass(cmd::parse_sass::Cmd),
    GenerateCss(cmd::css::Cmd),
}

impl Opts {
    fn run(self) -> Result<()> {
        self.sub.run()
    }
}

impl Subcommand {
    fn run(self) -> Result<()> {
        match self {
            Self::Internal(i) => i.run(),
            Self::Version => {
                println!("devel");
                Ok(())
            }
        }
    }
}

impl InternalW {
    fn run(self) -> Result<()> {
        self.sub.run()
    }
}

impl Internal {
    fn run(self) -> Result<()> {
        match self {
            Self::ParseSummary(c) => c.run(),
            Self::Page(c) => c.run(),
            Self::ParseTemplate(c) => c.run(),
            Self::ParseSass(c) => c.run(),
            Self::GenerateCss(c) => c.run(),
        }
    }
}

fn main() -> Result<()> {
    color_eyre::install()?;
    // for _ in 0..100 {
    let opts = Opts::parse();
    opts.run()
    // }
    // Ok(())
}
