// Copyright 2021 The Rust Project Developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// Taken from src/librustdoc/html/markdown.rs
// and src/librustdoc/html/toc.rs

// TODO: Understand this

use std::collections::{HashMap, VecDeque};

use std::ops::Range;

use pulldown_cmark::html::{self};
use pulldown_cmark::{Event, Tag};

type SpannedEvent<'a> = (Event<'a>, Range<usize>);

type IdMap = HashMap<String, usize>;

/// Make headings links with anchor IDs and build up TOC.
pub struct HeadingLinks<'a, 'b, I> {
    inner: I,
    toc: Option<&'b mut TocBuilder>,
    buf: VecDeque<SpannedEvent<'a>>,
    id_map: IdMap,
    title: &'b mut Option<String>,
}

impl<'a, 'b, I> HeadingLinks<'a, 'b, I> {
    pub fn new(iter: I, toc: Option<&'b mut TocBuilder>, title: &'b mut Option<String>) -> Self {
        HeadingLinks {
            inner: iter,
            toc,
            title,
            buf: VecDeque::new(),
            id_map: Default::default(),
        }
    }
}

impl<'a, 'b, I: Iterator<Item = SpannedEvent<'a>>> Iterator for HeadingLinks<'a, 'b, I> {
    type Item = SpannedEvent<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(e) = self.buf.pop_front() {
            return Some(e);
        }

        let event = self.inner.next();
        if let Some((Event::Start(Tag::Heading(level)), _)) = event {
            let mut id = String::new();
            for event in &mut self.inner {
                match &event.0 {
                    Event::End(Tag::Heading(..)) => break,
                    Event::Start(Tag::Link(_, _, _)) | Event::End(Tag::Link(..)) => {}
                    Event::Text(text) | Event::Code(text) => {
                        id.extend(text.chars().filter_map(slugify));
                        self.buf.push_back(event);
                    }
                    _ => self.buf.push_back(event),
                }
            }
            let id = get_id(&mut self.id_map, id);

            if let Some(ref mut builder) = self.toc {
                let mut html_header = String::new();
                html::push_html(&mut html_header, self.buf.iter().map(|(ev, _)| ev.clone()));
                if level == 1 {
                    // TODO: Check this is NONE (ie no previous header)
                    self.title.replace(html_header);
                } else {
                    builder.push(level as u32 - 1, html_header, id.clone());
                }

                // let sec = builder.push(level as u32, html_header, id.clone());
                // self.buf
                //     .push_front((Event::Html(format!("{} ", sec).into()), 0..0));
            }

            self.buf
                .push_back((Event::Html(format!("</a></h{}>", level).into()), 0..0));

            let start_tags = format!(
                "<h{level} id=\"{id}\">\
                    <a href=\"#{id}\" class=\"autolink\">",
                id = id,
                level = level
            );
            return Some((Event::Html(start_tags.into()), 0..0));
        }
        event
    }
}

fn get_id(m: &mut IdMap, s: String) -> String {
    let id = match m.get_mut(&s) {
        None => s,
        Some(num) => {
            let id = format!("{}-{}", s, num);
            *num += 1;
            id
        }
    };
    m.insert(id.clone(), 1);
    id
}

/// Convert chars from a title for an id.
///
/// "Hello, world!" -> "hello-world"
fn slugify(c: char) -> Option<char> {
    if c.is_alphanumeric() || c == '-' || c == '_' {
        if c.is_ascii() {
            Some(c.to_ascii_lowercase())
        } else {
            Some(c)
        }
    } else if c.is_whitespace() && c.is_ascii() {
        Some('-')
    } else {
        None
    }
}

// Table-of-contents creation.

/// A (recursive) table of contents
#[derive(Debug, PartialEq)]
pub struct Toc {
    /// The levels are strictly decreasing, i.e.
    ///
    /// `entries[0].level >= entries[1].level >= ...`
    ///
    /// Normally they are equal, but can differ in cases like A and B,
    /// both of which end up in the same `Toc` as they have the same
    /// parent (Main).
    ///
    /// ```text
    /// # Main
    /// ### A
    /// ## B
    /// ```
    entries: Vec<TocEntry>,
}

impl Toc {
    fn count_entries_with_level(&self, level: u32) -> usize {
        self.entries.iter().filter(|e| e.level == level).count()
    }
}

#[derive(Debug, PartialEq)]
struct TocEntry {
    level: u32,
    sec_number: String,
    name: String,
    id: String,
    children: Toc,
}

/// Progressive construction of a table of contents.
#[derive(Debug, PartialEq)]
pub struct TocBuilder {
    top_level: Toc,
    /// The current hierarchy of parent headings, the levels are
    /// strictly increasing (i.e., `chain[0].level < chain[1].level <
    /// ...`) with each entry being the most recent occurrence of a
    /// heading with that level (it doesn't include the most recent
    /// occurrences of every level, just, if it *is* in `chain` then
    /// it is the most recent one).
    ///
    /// We also have `chain[0].level <= top_level.entries[last]`.
    chain: Vec<TocEntry>,
}

impl TocBuilder {
    pub fn new() -> TocBuilder {
        TocBuilder {
            top_level: Toc {
                entries: Vec::new(),
            },
            chain: Vec::new(),
        }
    }

    /// Converts into a true `Toc` struct.
    pub fn into_toc(mut self) -> Toc {
        // we know all levels are >= 1.
        self.fold_until(0);
        self.top_level
    }

    /// Collapse the chain until the first heading more important than
    /// `level` (i.e., lower level)
    ///
    /// Example:
    ///
    /// ```text
    /// ## A
    /// # B
    /// # C
    /// ## D
    /// ## E
    /// ### F
    /// #### G
    /// ### H
    /// ```
    ///
    /// If we are considering H (i.e., level 3), then A and B are in
    /// self.top_level, D is in C.children, and C, E, F, G are in
    /// self.chain.
    ///
    /// When we attempt to push H, we realize that first G is not the
    /// parent (level is too high) so it is popped from chain and put
    /// into F.children, then F isn't the parent (level is equal, aka
    /// sibling), so it's also popped and put into E.children.
    ///
    /// This leaves us looking at E, which does have a smaller level,
    /// and, by construction, it's the most recent thing with smaller
    /// level, i.e., it's the immediate parent of H.
    fn fold_until(&mut self, level: u32) {
        let mut this = None;
        loop {
            match self.chain.pop() {
                Some(mut next) => {
                    next.children.entries.extend(this);
                    if next.level < level {
                        // this is the parent we want, so return it to
                        // its rightful place.
                        self.chain.push(next);
                        return;
                    } else {
                        this = Some(next);
                    }
                }
                None => {
                    self.top_level.entries.extend(this);
                    return;
                }
            }
        }
    }

    /// Push a level `level` heading into the appropriate place in the
    /// hierarchy, returning a string containing the section number in
    /// `<num>.<num>.<num>` format.
    fn push(&mut self, level: u32, name: String, id: String) -> &str {
        assert!(level >= 1);

        // collapse all previous sections into their parents until we
        // get to relevant heading (i.e., the first one with a smaller
        // level than us)
        self.fold_until(level);

        let mut sec_number;
        {
            let (toc_level, toc) = match self.chain.last() {
                None => {
                    sec_number = String::new();
                    (0, &self.top_level)
                }
                Some(entry) => {
                    sec_number = entry.sec_number.clone();
                    sec_number.push('.');
                    (entry.level, &entry.children)
                }
            };
            // fill in any missing zeros, e.g., for
            // # Foo (1)
            // ### Bar (1.0.1)
            for _ in toc_level..level - 1 {
                sec_number.push_str("0.");
            }
            let number = toc.count_entries_with_level(level);
            sec_number.push_str(&(number + 1).to_string())
        }

        self.chain.push(TocEntry {
            level,
            name,
            sec_number,
            id,
            children: Toc {
                entries: Vec::new(),
            },
        });

        // get the thing we just pushed, so we can borrow the string
        // out of it with the right lifetime
        let just_inserted = self.chain.last_mut().unwrap();
        &just_inserted.sec_number
    }
}

impl Toc {
    fn print_inner(&self, v: &mut String) {
        v.push_str("<ol>");
        for entry in &self.entries {
            // recursively format this table of contents
            // TODO: decide if I want the numbers
            v.push_str(&format!(
                "\n<li><a href=\"#{id}\"><b>{num}</b> {name}</a>",
                id = entry.id,
                num = entry.sec_number,
                name = entry.name
            ));
            entry.children.print_inner(&mut *v);
            v.push_str("</li>");
        }
        v.push_str("</ol>");
    }
    pub fn print(&self) -> String {
        let mut v = String::new();
        self.print_inner(&mut v);
        v
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_id() {
        let mut idmat = IdMap::new();
        for (insert, expected) in &[
            ("f", "f"),
            ("f", "f-1"),
            ("f-1", "f-1-1"),
            ("f", "f-2"),
            ("f-2", "f-2-1"),
        ] {
            assert_eq!(&get_id(&mut idmat, (*insert).to_owned()), *expected)
        }
    }

    #[test]
    fn builder_smoke() {
        let mut builder = TocBuilder::new();

        // this is purposely not using a fancy macro like below so
        // that we're sure that this is doing the correct thing, and
        // there's been no macro mistake.
        macro_rules! push {
            ($level: expr, $name: expr) => {
                assert_eq!(
                    builder.push($level, $name.to_string(), "".to_string()),
                    $name
                );
            };
        }
        push!(2, "0.1");
        push!(1, "1");
        {
            push!(2, "1.1");
            {
                push!(3, "1.1.1");
                push!(3, "1.1.2");
            }
            push!(2, "1.2");
            {
                push!(3, "1.2.1");
                push!(3, "1.2.2");
            }
        }
        push!(1, "2");
        push!(1, "3");
        {
            push!(4, "3.0.0.1");
            {
                push!(6, "3.0.0.1.0.1");
            }
            push!(4, "3.0.0.2");
            push!(2, "3.1");
            {
                push!(4, "3.1.0.1");
            }
        }

        macro_rules! toc {
        ($(($level: expr, $name: expr, $(($sub: tt))* )),*) => {
            Toc {
                entries: vec![
                    $(
                        TocEntry {
                            level: $level,
                            name: $name.to_string(),
                            sec_number: $name.to_string(),
                            id: "".to_string(),
                            children: toc!($($sub),*)
                        }
                        ),*
                    ]
            }
        }
    }
        let expected = toc!(
            (2, "0.1",),
            (
                1,
                "1",
                ((2, "1.1", ((3, "1.1.1",))((3, "1.1.2",))))((
                    2,
                    "1.2",
                    ((3, "1.2.1",))((3, "1.2.2",))
                ))
            ),
            (1, "2",),
            (
                1,
                "3",
                ((4, "3.0.0.1", ((6, "3.0.0.1.0.1",))))((4, "3.0.0.2",))((
                    2,
                    "3.1",
                    ((4, "3.1.0.1",))
                ))
            )
        );
        assert_eq!(expected, builder.into_toc());
    }
}
