use clap::Clap;
use eyre::Result;
use syntect::highlighting::ThemeSet;
use syntect::html::{css_for_theme_with_class_style, ClassStyle};

#[derive(Clap)]
/// Various CSS Related things
pub struct Cmd {
    #[clap(subcommand)]
    what: What,
}

#[derive(Clap)]
enum What {
    /// Print possible themes
    Themes,
    /// Print the CSS
    Css,
    /// Print the langs we know how to highlight
    Langs,
}

// TODO: Decide how I want to do this
// c r -- internal generate-css > www/highlight.scss

pub const CLASS_STYLE: ClassStyle = ClassStyle::SpacedPrefixed { prefix: "xms" };

impl Cmd {
    pub fn run(self) -> Result<()> {
        match self.what {
            What::Themes => {
                let themes = ThemeSet::load_defaults();
                for (n, _) in themes.themes {
                    println!("{}", n)
                }
            }
            What::Css => {
                let themes = ThemeSet::load_defaults();
                // TODO: Pick the best theme

                let theme = &themes.themes["base16-ocean.light"];
                let css = css_for_theme_with_class_style(theme, CLASS_STYLE);
                println!("{}", css);
            }
            What::Langs => {
                let ssets = bat::assets::HighlightingAssets::get_integrated_syntaxset();
                for i in ssets.syntaxes() {
                    println!("{}", i.name);
                }
            }
        }

        Ok(())
    }
}
