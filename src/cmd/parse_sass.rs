use std::fs;
use std::path::PathBuf;

use clap::Clap;
use eyre::{Context, Result};
use rsass::output::{Format, Style};

#[derive(Clap)]
/// Parse a .sass file to a .css file
pub struct Cmd {
    #[clap(long = "in")]
    sass: PathBuf,
    #[clap(long = "out")]
    css: PathBuf,
}

impl Cmd {
    pub fn run(self) -> Result<()> {
        let css = rsass::compile_scss_file(
            &self.sass,
            Format {
                style: Style::Compressed,
                precision: 5,
            },
        )
        .wrap_err_with(|| format!("Failed to parse sass of {:?}", self.sass))?;

        fs::write(&self.css, css).wrap_err_with(|| format!("Failed to write to {:?}", self.css))?;

        Ok(())
    }
}
