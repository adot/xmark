// https://github.com/rust-lang/rust/blob/master/src/librustdoc/html/markdown.rs
// TODO: License

use std::fs;
use std::io::BufReader;

use std::path::PathBuf;

use clap::Clap;
use eyre::{Context, Result};
use pulldown_cmark::html::push_html;
use pulldown_cmark::{Options, Parser};
use serde_json::json;
use telemachus::Template;

use crate::mdutil::{HeadingLinks, Toc, TocBuilder};
use crate::summary::parse_summary;
use crate::{highlight, ltoc};

#[derive(Debug, Clap, Clone, PartialEq)]
/// Render a main page
pub struct Cmd {
    #[clap(long = "in")]
    input: PathBuf,
    #[clap(long = "out")]
    output: PathBuf,
    #[clap(long)]
    template: PathBuf,
    #[clap(long)]
    title: String,
    #[clap(long)]
    summary: PathBuf,
    #[clap(long)]
    base_url: String,
}

impl Cmd {
    pub fn run(self) -> Result<()> {
        let tpl: Template =
            bincode::deserialize_from(BufReader::new(fs::File::open(&self.template)?))
                .wrap_err_with(|| format!("Failed to read from {:?}", &self.template))?;

        let md_content = fs::read_to_string(&self.input)?;

        // TODO: Reuse this io with cmd::parse_summary
        let markdown = fs::read_to_string(&self.summary)
            .wrap_err_with(|| format!("Couldnt read {:?}", &self.summary))?;
        let summary = parse_summary(&markdown)
            .wrap_err_with(|| format!("Couldn't parse {:?}", &self.summary))?;

        let ltoc = ltoc::toc(&summary, &self.base_url, &self.input)?;
        let (content, rtoc, title) = html(&md_content)
            .wrap_err_with(|| format!("Failed to generate html for {:?}", &self.input))?;

        // TODO: Figure out title
        // Currently, Summary.md is for the LHSTOC & the html <title>
        // And the first H1 is for the top title and the RHSTOC
        let vals = json!({
            "title": self.title,
            "content": content,
            "ltoc": ltoc,
            "rtoc": rtoc.print(),
            "rhstitle": title.unwrap_or_default()
        });

        let output = tpl.render(vals)?;

        fs::write(self.output, output)?;

        Ok(())
    }
}

// TODO: Dont have the return type be a mess
// TODO: Move to its own file
/// Returns main HTML, RTOC and Title
fn html(inp: &str) -> Result<(String, Toc, Option<String>)> {
    let p = Parser::new_ext(inp, Options::all());
    let p = p.into_offset_iter();

    let mut toc_builder = TocBuilder::new();

    let mut title = None;

    let p = HeadingLinks::new(p, Some(&mut toc_builder), &mut title);

    // Discard the the spans
    let p = p.map(|(e, _)| e);

    let p = highlight::Hightlighter::new(p);

    // TODO: Don't buffer the input
    // but bail from push_html if an Err comes up.
    // this will require a PR to pulldown-cmark
    let p = p
        .collect::<Result<Vec<_>, _>>()
        .wrap_err("Failed to highlight")?;

    let mut ret = String::with_capacity(inp.len() * 3 / 2);

    push_html(&mut ret, p.into_iter());

    let toc = toc_builder.into_toc();
    Ok((ret, toc, title))
}
