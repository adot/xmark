use std::fs;
use std::path::PathBuf;

use clap::Clap;
use eyre::{Result, WrapErr};

use crate::summary::parse_summary;

#[derive(Debug, Clap, Clone, PartialEq)]
/// Parse SUMMARY.md to json
pub struct Cmd {
    #[clap(long)]
    markdown_in: PathBuf,
    #[clap(long)]
    json_out: PathBuf,
}

impl Cmd {
    pub fn run(self) -> Result<()> {
        let markdown = fs::read_to_string(&self.markdown_in)
            .wrap_err_with(|| format!("Couldnt read {:?}", &self.markdown_in))?;
        let summary = parse_summary(&markdown)
            .wrap_err_with(|| format!("Couldn't parse {:?}", &self.markdown_in))?;
        let json = serde_json::to_string(&summary)?;
        fs::write(&self.json_out, json)
            .wrap_err_with(|| format!("Couldnt write to {:?}", &self.json_out))?;

        Ok(())
    }
}
