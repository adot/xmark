test:
	pytest ninjen tests
	cargo test -q

fmt:
	black ninjen tests
	cargo fmt
	./node_modules/.bin/js-beautify -r www/*.html
	./node_modules/.bin/prettier -w www/*.scss
	./node_modules/.bin/prettier -w www/*.js

rust-build:
	cargo build

clean:
	ninja -C corpus/_build -t clean

clean-hard:
	rm -rf corpus/_build

gen:
	python ninjen/ninjen.py corpus

build:
	ninja -C corpus/_build | cat

lint:
	flake8 tests ninjen

check-all: rust-build fmt lint test

serve: build
	python -m http.server -d corpus/_build/html

bench: gen
	hyperfine "ninja -C corpus/_build -t clean && ninja -C corpus/_build"

doc: 
	cargo doc --document-private-items

.PHONY: test fmt clean gen build clean-hard lint rust-build doc
