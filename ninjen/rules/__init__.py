# TODO: Remove this
# flake8: noqa


# TODO: learn how python imprts work
import rules.page
import rules.regenerate
import rules.build_template
import rules.copy
import rules.build_css


def add_all(n):
    # Now THIS is metaprogramming
    for i in [page, regenerate, build_template, copy, build_css]:
        i.add(n)
