RULE_COPY = "copy"

VAR_FROM_PRITTY = "in_pritty"
VAR_TO_PRITTY = "out_pritty"


def add(ninja):
    ninja.rule(RULE_COPY, "cp $in $out", f"Copy ${VAR_FROM_PRITTY} => ${VAR_TO_PRITTY}")


def invoke(n, from_, to, fp=None, tp=None):
    if fp is None:
        fp = from_
    if tp is None:
        tp = to

    n.build(to, RULE_COPY, from_, variables={VAR_FROM_PRITTY: fp, VAR_TO_PRITTY: tp})
