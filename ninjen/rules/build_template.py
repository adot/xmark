from vars import VAR_XMARK_EXE

RULE_TEMPLATE = "build_template"
VAR_NAME = "tpl_name"

# TODO: Test
def add(ninja):
    ninja.rule(
        RULE_TEMPLATE,
        f"${VAR_XMARK_EXE} internal parse-template --in=$in --out=$out",
        f"Parsing template ${VAR_NAME}",
    )


def invoke(ninja, inp, out, pritty=None):
    if pritty is None:
        pritty = inp
    ninja.build(out, RULE_TEMPLATE, inp, variables={VAR_NAME: pritty})
