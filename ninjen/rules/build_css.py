from vars import VAR_XMARK_EXE

RULE_SASS = "build_css"
VAR_NAME = "sass_name"
VAR_PRIMARY = "sass_main"

# TODO: Test
def add(ninja):
    ninja.rule(
        RULE_SASS,
        f"${VAR_XMARK_EXE} internal parse-sass --in=${VAR_PRIMARY} --out=$out",
        f"Parsing sass ${VAR_NAME}",
    )


# We must staticly know all the @inports of the sass
def invoke(ninja, sass, css, imports=[], pritty=None):
    if pritty is None:
        pritty = sass

    ninja.build(
        css,
        RULE_SASS,
        imports + [sass],
        variables={VAR_NAME: pritty, VAR_PRIMARY: sass},
    )
