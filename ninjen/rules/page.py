from vars import VAR_XMARK_EXE

RULE_PAGE = "build_page"
# "Pritty" (non absolute) input loc for description
VAR_LOC_PRITTY = "in_location"
VAR_IN_MD = "in_md"
VAR_TPL = "template"
VAR_TITLE = "title"
VAR_OUT_HTML = "html"

# Global vars
VAR_SUMMARY = "this_book_summary"
VAR_BASE_URL = "this_book_baseurl"


def invoke(ninja, in_md, out_html, title, template, pritty_location=None):
    if pritty_location is None:
        pritty_location = in_md

    ninja.build(
        out_html,
        RULE_PAGE,
        # Rebuild if xmark is new.
        # TODO:
        [in_md, template, f"${VAR_XMARK_EXE}", f"${VAR_SUMMARY}"],
        variables={
            VAR_IN_MD: in_md,
            VAR_LOC_PRITTY: pritty_location,
            VAR_OUT_HTML: out_html,
            VAR_TITLE: title,
            VAR_TPL: template,
        },
    )


def add(ninja):
    ninja.rule(
        RULE_PAGE,
        f'${VAR_XMARK_EXE} internal page \
            --in=${VAR_IN_MD} \
            --out=${VAR_OUT_HTML} \
            --title="${VAR_TITLE}" \
            --template=${VAR_TPL} \
            --summary=${VAR_SUMMARY} \
            --base-url=${VAR_BASE_URL}',
        description=f"Building ${VAR_LOC_PRITTY}",
    )
