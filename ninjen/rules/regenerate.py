from vars import VAR_PY_EXE, VAR_NINJEN_SRC, VAR_BASE_DIR, NINJA_FILE

RULE_REGENRATE = "regenerate"


def add(ninja):
    ninja.rule(
        RULE_REGENRATE,
        f"${VAR_PY_EXE} ${VAR_NINJEN_SRC} ${VAR_BASE_DIR}",
        "Regererating build files.",
        generator=True,
    )


def invoke(ninja, trigers_redo):
    ninja.build(NINJA_FILE, RULE_REGENRATE, trigers_redo, pool="console")
