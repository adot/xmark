import tomlkit
import typing
from dataclasses import dataclass


@dataclass
class XmarkToml:
    # Name, path
    books: list[typing.Tuple[str, str]]
    rest: typing.Any


def parse(text):
    toml = tomlkit.parse(text)
    books = toml["books"]
    toml.remove("books")

    return XmarkToml(list(map(_extract_item, books)), toml)


def _extract_item(item):
    if isinstance(item, str):
        return item, item
    else:
        return item["name"], item["location"]
