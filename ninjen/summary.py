from dataclasses import dataclass
import typing


def parse(summary):
    yield from _parse_part(summary["prefix_chapters"])
    yield from _parse_part(summary["numbered_chapters"])
    yield from _parse_part(summary["suffix_chapters"])


def _parse_part(part):
    for i in part:
        if isinstance(i, str):
            yield from []
            return

        if inner := i.get("Link"):
            yield _parse_link(inner)
            yield from _parse_part(inner["nested_items"])


@dataclass
class Item:
    title: str
    location: typing.Optional[str]
    number: typing.Optional[typing.List[int]]


def _parse_link(link):
    title = link["name"]
    location = link["location"]
    number = link["number"]
    return Item(title, location, number)
