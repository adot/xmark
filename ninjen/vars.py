from os import path


USE_RELEASE = False

XM_TYPE = "debug" if not USE_RELEASE else "release"

XMARK_EXE = path.join(path.dirname(path.dirname(__file__)), f"target/{XM_TYPE}/xmark")
BUILD_DIR_NAME = "_build"
TOML_FILE_NAME = "xmark.toml"
TPL_DIR_NAME = "templates"
TPL_SRC_DIR = path.join(path.dirname(path.dirname(__file__)), "www")

VAR_XMARK_EXE = "xmark"
VAR_BUILD_DIR = "build_dir"
VAR_BASE_DIR = "book_basedir"
VAR_PY_EXE = "python"
VAR_NINJEN_SRC = "ninjen"
VAR_BOOK_LOC = "book_rel_loc"
VAR_BOOK_NAME = "book_name"
VAR_TPL_DIR = "tpl_dir"

NINJA_FILE = "build.ninja"
