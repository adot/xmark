{
  "title": "The Rust Programming Language",
  "prefix_chapters": [
    {
      "Link": {
        "name": "The Rust Programming Language",
        "location": "title-page.md",
        "number": null,
        "nested_items": []
      }
    },
    {
      "Link": {
        "name": "Foreword",
        "location": "foreword.md",
        "number": null,
        "nested_items": []
      }
    },
    {
      "Link": {
        "name": "Introduction",
        "location": "ch00-00-introduction.md",
        "number": null,
        "nested_items": []
      }
    }
  ],
  "numbered_chapters": [
    {
      "Link": {
        "name": "Getting Started",
        "location": "ch01-00-getting-started.md",
        "number": [
          1
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Installation",
              "location": "ch01-01-installation.md",
              "number": [
                1,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Hello, World!",
              "location": "ch01-02-hello-world.md",
              "number": [
                1,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Hello, Cargo!",
              "location": "ch01-03-hello-cargo.md",
              "number": [
                1,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Programming a Guessing Game",
        "location": "ch02-00-guessing-game-tutorial.md",
        "number": [
          2
        ],
        "nested_items": []
      }
    },
    {
      "Link": {
        "name": "Common Programming Concepts",
        "location": "ch03-00-common-programming-concepts.md",
        "number": [
          3
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Variables and Mutability",
              "location": "ch03-01-variables-and-mutability.md",
              "number": [
                3,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Data Types",
              "location": "ch03-02-data-types.md",
              "number": [
                3,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Functions",
              "location": "ch03-03-how-functions-work.md",
              "number": [
                3,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Comments",
              "location": "ch03-04-comments.md",
              "number": [
                3,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Control Flow",
              "location": "ch03-05-control-flow.md",
              "number": [
                3,
                5
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Understanding Ownership",
        "location": "ch04-00-understanding-ownership.md",
        "number": [
          4
        ],
        "nested_items": [
          {
            "Link": {
              "name": "What is Ownership?",
              "location": "ch04-01-what-is-ownership.md",
              "number": [
                4,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "References and Borrowing",
              "location": "ch04-02-references-and-borrowing.md",
              "number": [
                4,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "The Slice Type",
              "location": "ch04-03-slices.md",
              "number": [
                4,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Using Structs to Structure Related Data",
        "location": "ch05-00-structs.md",
        "number": [
          5
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Defining and Instantiating Structs",
              "location": "ch05-01-defining-structs.md",
              "number": [
                5,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "An Example Program Using Structs",
              "location": "ch05-02-example-structs.md",
              "number": [
                5,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Method Syntax",
              "location": "ch05-03-method-syntax.md",
              "number": [
                5,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Enums and Pattern Matching",
        "location": "ch06-00-enums.md",
        "number": [
          6
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Defining an Enum",
              "location": "ch06-01-defining-an-enum.md",
              "number": [
                6,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "The match Control Flow Operator",
              "location": "ch06-02-match.md",
              "number": [
                6,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Concise Control Flow with if let",
              "location": "ch06-03-if-let.md",
              "number": [
                6,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Managing Growing Projects with Packages, Crates, and Modules",
        "location": "ch07-00-managing-growing-projects-with-packages-crates-and-modules.md",
        "number": [
          7
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Packages and Crates",
              "location": "ch07-01-packages-and-crates.md",
              "number": [
                7,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Defining Modules to Control Scope and Privacy",
              "location": "ch07-02-defining-modules-to-control-scope-and-privacy.md",
              "number": [
                7,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Paths for Referring to an Item in the Module Tree",
              "location": "ch07-03-paths-for-referring-to-an-item-in-the-module-tree.md",
              "number": [
                7,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Bringing Paths Into Scope with the use Keyword",
              "location": "ch07-04-bringing-paths-into-scope-with-the-use-keyword.md",
              "number": [
                7,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Separating Modules into Different Files",
              "location": "ch07-05-separating-modules-into-different-files.md",
              "number": [
                7,
                5
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Common Collections",
        "location": "ch08-00-common-collections.md",
        "number": [
          8
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Storing Lists of Values with Vectors",
              "location": "ch08-01-vectors.md",
              "number": [
                8,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Storing UTF-8 Encoded Text with Strings",
              "location": "ch08-02-strings.md",
              "number": [
                8,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Storing Keys with Associated Values in Hash Maps",
              "location": "ch08-03-hash-maps.md",
              "number": [
                8,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Error Handling",
        "location": "ch09-00-error-handling.md",
        "number": [
          9
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Unrecoverable Errors with panic!",
              "location": "ch09-01-unrecoverable-errors-with-panic.md",
              "number": [
                9,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Recoverable Errors with Result",
              "location": "ch09-02-recoverable-errors-with-result.md",
              "number": [
                9,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "To panic! or Not To panic!",
              "location": "ch09-03-to-panic-or-not-to-panic.md",
              "number": [
                9,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Generic Types, Traits, and Lifetimes",
        "location": "ch10-00-generics.md",
        "number": [
          10
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Generic Data Types",
              "location": "ch10-01-syntax.md",
              "number": [
                10,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Traits: Defining Shared Behavior",
              "location": "ch10-02-traits.md",
              "number": [
                10,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Validating References with Lifetimes",
              "location": "ch10-03-lifetime-syntax.md",
              "number": [
                10,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Writing Automated Tests",
        "location": "ch11-00-testing.md",
        "number": [
          11
        ],
        "nested_items": [
          {
            "Link": {
              "name": "How to Write Tests",
              "location": "ch11-01-writing-tests.md",
              "number": [
                11,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Controlling How Tests Are Run",
              "location": "ch11-02-running-tests.md",
              "number": [
                11,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Test Organization",
              "location": "ch11-03-test-organization.md",
              "number": [
                11,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "An I/O Project: Building a Command Line Program",
        "location": "ch12-00-an-io-project.md",
        "number": [
          12
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Accepting Command Line Arguments",
              "location": "ch12-01-accepting-command-line-arguments.md",
              "number": [
                12,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Reading a File",
              "location": "ch12-02-reading-a-file.md",
              "number": [
                12,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Refactoring to Improve Modularity and Error Handling",
              "location": "ch12-03-improving-error-handling-and-modularity.md",
              "number": [
                12,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Developing the Library’s Functionality with Test Driven Development",
              "location": "ch12-04-testing-the-librarys-functionality.md",
              "number": [
                12,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Working with Environment Variables",
              "location": "ch12-05-working-with-environment-variables.md",
              "number": [
                12,
                5
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Writing Error Messages to Standard Error Instead of Standard Output",
              "location": "ch12-06-writing-to-stderr-instead-of-stdout.md",
              "number": [
                12,
                6
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Functional Language Features: Iterators and Closures",
        "location": "ch13-00-functional-features.md",
        "number": [
          13
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Closures: Anonymous Functions that Can Capture Their Environment",
              "location": "ch13-01-closures.md",
              "number": [
                13,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Processing a Series of Items with Iterators",
              "location": "ch13-02-iterators.md",
              "number": [
                13,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Improving Our I/O Project",
              "location": "ch13-03-improving-our-io-project.md",
              "number": [
                13,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Comparing Performance: Loops vs. Iterators",
              "location": "ch13-04-performance.md",
              "number": [
                13,
                4
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "More about Cargo and Crates.io",
        "location": "ch14-00-more-about-cargo.md",
        "number": [
          14
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Customizing Builds with Release Profiles",
              "location": "ch14-01-release-profiles.md",
              "number": [
                14,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Publishing a Crate to Crates.io",
              "location": "ch14-02-publishing-to-crates-io.md",
              "number": [
                14,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Cargo Workspaces",
              "location": "ch14-03-cargo-workspaces.md",
              "number": [
                14,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Installing Binaries from Crates.io with cargo install",
              "location": "ch14-04-installing-binaries.md",
              "number": [
                14,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Extending Cargo with Custom Commands",
              "location": "ch14-05-extending-cargo.md",
              "number": [
                14,
                5
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Smart Pointers",
        "location": "ch15-00-smart-pointers.md",
        "number": [
          15
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Using Box<T> to Point to Data on the Heap",
              "location": "ch15-01-box.md",
              "number": [
                15,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Treating Smart Pointers Like Regular References with the Deref Trait",
              "location": "ch15-02-deref.md",
              "number": [
                15,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Running Code on Cleanup with the Drop Trait",
              "location": "ch15-03-drop.md",
              "number": [
                15,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Rc<T>, the Reference Counted Smart Pointer",
              "location": "ch15-04-rc.md",
              "number": [
                15,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "RefCell<T> and the Interior Mutability Pattern",
              "location": "ch15-05-interior-mutability.md",
              "number": [
                15,
                5
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Reference Cycles Can Leak Memory",
              "location": "ch15-06-reference-cycles.md",
              "number": [
                15,
                6
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Fearless Concurrency",
        "location": "ch16-00-concurrency.md",
        "number": [
          16
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Using Threads to Run Code Simultaneously",
              "location": "ch16-01-threads.md",
              "number": [
                16,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Using Message Passing to Transfer Data Between Threads",
              "location": "ch16-02-message-passing.md",
              "number": [
                16,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Shared-State Concurrency",
              "location": "ch16-03-shared-state.md",
              "number": [
                16,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Extensible Concurrency with the Sync and Send Traits",
              "location": "ch16-04-extensible-concurrency-sync-and-send.md",
              "number": [
                16,
                4
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Object Oriented Programming Features of Rust",
        "location": "ch17-00-oop.md",
        "number": [
          17
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Characteristics of Object-Oriented Languages",
              "location": "ch17-01-what-is-oo.md",
              "number": [
                17,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Using Trait Objects That Allow for Values of Different Types",
              "location": "ch17-02-trait-objects.md",
              "number": [
                17,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Implementing an Object-Oriented Design Pattern",
              "location": "ch17-03-oo-design-patterns.md",
              "number": [
                17,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Patterns and Matching",
        "location": "ch18-00-patterns.md",
        "number": [
          18
        ],
        "nested_items": [
          {
            "Link": {
              "name": "All the Places Patterns Can Be Used",
              "location": "ch18-01-all-the-places-for-patterns.md",
              "number": [
                18,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Refutability: Whether a Pattern Might Fail to Match",
              "location": "ch18-02-refutability.md",
              "number": [
                18,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Pattern Syntax",
              "location": "ch18-03-pattern-syntax.md",
              "number": [
                18,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Advanced Features",
        "location": "ch19-00-advanced-features.md",
        "number": [
          19
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Unsafe Rust",
              "location": "ch19-01-unsafe-rust.md",
              "number": [
                19,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Advanced Traits",
              "location": "ch19-03-advanced-traits.md",
              "number": [
                19,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Advanced Types",
              "location": "ch19-04-advanced-types.md",
              "number": [
                19,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Advanced Functions and Closures",
              "location": "ch19-05-advanced-functions-and-closures.md",
              "number": [
                19,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Macros",
              "location": "ch19-06-macros.md",
              "number": [
                19,
                5
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Final Project: Building a Multithreaded Web Server",
        "location": "ch20-00-final-project-a-web-server.md",
        "number": [
          20
        ],
        "nested_items": [
          {
            "Link": {
              "name": "Building a Single-Threaded Web Server",
              "location": "ch20-01-single-threaded.md",
              "number": [
                20,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Turning Our Single-Threaded Server into a Multithreaded Server",
              "location": "ch20-02-multithreaded.md",
              "number": [
                20,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "Graceful Shutdown and Cleanup",
              "location": "ch20-03-graceful-shutdown-and-cleanup.md",
              "number": [
                20,
                3
              ],
              "nested_items": []
            }
          }
        ]
      }
    },
    {
      "Link": {
        "name": "Appendix",
        "location": "appendix-00.md",
        "number": [
          21
        ],
        "nested_items": [
          {
            "Link": {
              "name": "A - Keywords",
              "location": "appendix-01-keywords.md",
              "number": [
                21,
                1
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "B - Operators and Symbols",
              "location": "appendix-02-operators.md",
              "number": [
                21,
                2
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "C - Derivable Traits",
              "location": "appendix-03-derivable-traits.md",
              "number": [
                21,
                3
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "D - Useful Development Tools",
              "location": "appendix-04-useful-development-tools.md",
              "number": [
                21,
                4
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "E - Editions",
              "location": "appendix-05-editions.md",
              "number": [
                21,
                5
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "F - Translations of the Book",
              "location": "appendix-06-translation.md",
              "number": [
                21,
                6
              ],
              "nested_items": []
            }
          },
          {
            "Link": {
              "name": "G - How Rust is Made and “Nightly Rust”",
              "location": "appendix-07-nightly-rust.md",
              "number": [
                21,
                7
              ],
              "nested_items": []
            }
          }
        ]
      }
    }
  ],
  "suffix_chapters": []
}