import unittest
from os import path
import json

# Help, I have literaly no idea how pythons import system works
from ninjen import summary
from ninjen.summary import Item


TEST_DATA_DIR = path.join(path.dirname(__file__), "test_data")

TRPL = [
    Item(title="The Rust Programming Language", location="title-page.md", number=None),
    Item(title="Foreword", location="foreword.md", number=None),
    Item(title="Introduction", location="ch00-00-introduction.md", number=None),
    Item(title="Getting Started", location="ch01-00-getting-started.md", number=[1]),
    Item(title="Installation", location="ch01-01-installation.md", number=[1, 1]),
    Item(title="Hello, World!", location="ch01-02-hello-world.md", number=[1, 2]),
    Item(title="Hello, Cargo!", location="ch01-03-hello-cargo.md", number=[1, 3]),
    Item(
        title="Programming a Guessing Game",
        location="ch02-00-guessing-game-tutorial.md",
        number=[2],
    ),
    Item(
        title="Common Programming Concepts",
        location="ch03-00-common-programming-concepts.md",
        number=[3],
    ),
    Item(
        title="Variables and Mutability",
        location="ch03-01-variables-and-mutability.md",
        number=[3, 1],
    ),
    Item(title="Data Types", location="ch03-02-data-types.md", number=[3, 2]),
    Item(title="Functions", location="ch03-03-how-functions-work.md", number=[3, 3]),
    Item(title="Comments", location="ch03-04-comments.md", number=[3, 4]),
    Item(title="Control Flow", location="ch03-05-control-flow.md", number=[3, 5]),
    Item(
        title="Understanding Ownership",
        location="ch04-00-understanding-ownership.md",
        number=[4],
    ),
    Item(
        title="What is Ownership?",
        location="ch04-01-what-is-ownership.md",
        number=[4, 1],
    ),
    Item(
        title="References and Borrowing",
        location="ch04-02-references-and-borrowing.md",
        number=[4, 2],
    ),
    Item(title="The Slice Type", location="ch04-03-slices.md", number=[4, 3]),
    Item(
        title="Using Structs to Structure Related Data",
        location="ch05-00-structs.md",
        number=[5],
    ),
    Item(
        title="Defining and Instantiating Structs",
        location="ch05-01-defining-structs.md",
        number=[5, 1],
    ),
    Item(
        title="An Example Program Using Structs",
        location="ch05-02-example-structs.md",
        number=[5, 2],
    ),
    Item(title="Method Syntax", location="ch05-03-method-syntax.md", number=[5, 3]),
    Item(title="Enums and Pattern Matching", location="ch06-00-enums.md", number=[6]),
    Item(
        title="Defining an Enum", location="ch06-01-defining-an-enum.md", number=[6, 1]
    ),
    Item(
        title="The match Control Flow Operator",
        location="ch06-02-match.md",
        number=[6, 2],
    ),
    Item(
        title="Concise Control Flow with if let",
        location="ch06-03-if-let.md",
        number=[6, 3],
    ),
    Item(
        title="Managing Growing Projects with Packages, Crates, and Modules",
        location="ch07-00-managing-growing-projects-with-packages-crates-and-modules.md",
        number=[7],
    ),
    Item(
        title="Packages and Crates",
        location="ch07-01-packages-and-crates.md",
        number=[7, 1],
    ),
    Item(
        title="Defining Modules to Control Scope and Privacy",
        location="ch07-02-defining-modules-to-control-scope-and-privacy.md",
        number=[7, 2],
    ),
    Item(
        title="Paths for Referring to an Item in the Module Tree",
        location="ch07-03-paths-for-referring-to-an-item-in-the-module-tree.md",
        number=[7, 3],
    ),
    Item(
        title="Bringing Paths Into Scope with the use Keyword",
        location="ch07-04-bringing-paths-into-scope-with-the-use-keyword.md",
        number=[7, 4],
    ),
    Item(
        title="Separating Modules into Different Files",
        location="ch07-05-separating-modules-into-different-files.md",
        number=[7, 5],
    ),
    Item(
        title="Common Collections", location="ch08-00-common-collections.md", number=[8]
    ),
    Item(
        title="Storing Lists of Values with Vectors",
        location="ch08-01-vectors.md",
        number=[8, 1],
    ),
    Item(
        title="Storing UTF-8 Encoded Text with Strings",
        location="ch08-02-strings.md",
        number=[8, 2],
    ),
    Item(
        title="Storing Keys with Associated Values in Hash Maps",
        location="ch08-03-hash-maps.md",
        number=[8, 3],
    ),
    Item(title="Error Handling", location="ch09-00-error-handling.md", number=[9]),
    Item(
        title="Unrecoverable Errors with panic!",
        location="ch09-01-unrecoverable-errors-with-panic.md",
        number=[9, 1],
    ),
    Item(
        title="Recoverable Errors with Result",
        location="ch09-02-recoverable-errors-with-result.md",
        number=[9, 2],
    ),
    Item(
        title="To panic! or Not To panic!",
        location="ch09-03-to-panic-or-not-to-panic.md",
        number=[9, 3],
    ),
    Item(
        title="Generic Types, Traits, and Lifetimes",
        location="ch10-00-generics.md",
        number=[10],
    ),
    Item(title="Generic Data Types", location="ch10-01-syntax.md", number=[10, 1]),
    Item(
        title="Traits: Defining Shared Behavior",
        location="ch10-02-traits.md",
        number=[10, 2],
    ),
    Item(
        title="Validating References with Lifetimes",
        location="ch10-03-lifetime-syntax.md",
        number=[10, 3],
    ),
    Item(title="Writing Automated Tests", location="ch11-00-testing.md", number=[11]),
    Item(
        title="How to Write Tests", location="ch11-01-writing-tests.md", number=[11, 1]
    ),
    Item(
        title="Controlling How Tests Are Run",
        location="ch11-02-running-tests.md",
        number=[11, 2],
    ),
    Item(
        title="Test Organization",
        location="ch11-03-test-organization.md",
        number=[11, 3],
    ),
    Item(
        title="An I/O Project: Building a Command Line Program",
        location="ch12-00-an-io-project.md",
        number=[12],
    ),
    Item(
        title="Accepting Command Line Arguments",
        location="ch12-01-accepting-command-line-arguments.md",
        number=[12, 1],
    ),
    Item(title="Reading a File", location="ch12-02-reading-a-file.md", number=[12, 2]),
    Item(
        title="Refactoring to Improve Modularity and Error Handling",
        location="ch12-03-improving-error-handling-and-modularity.md",
        number=[12, 3],
    ),
    Item(
        title="Developing the Library’s Functionality with Test Driven Development",
        location="ch12-04-testing-the-librarys-functionality.md",
        number=[12, 4],
    ),
    Item(
        title="Working with Environment Variables",
        location="ch12-05-working-with-environment-variables.md",
        number=[12, 5],
    ),
    Item(
        title="Writing Error Messages to Standard Error Instead of Standard Output",
        location="ch12-06-writing-to-stderr-instead-of-stdout.md",
        number=[12, 6],
    ),
    Item(
        title="Functional Language Features: Iterators and Closures",
        location="ch13-00-functional-features.md",
        number=[13],
    ),
    Item(
        title="Closures: Anonymous Functions that Can Capture Their Environment",
        location="ch13-01-closures.md",
        number=[13, 1],
    ),
    Item(
        title="Processing a Series of Items with Iterators",
        location="ch13-02-iterators.md",
        number=[13, 2],
    ),
    Item(
        title="Improving Our I/O Project",
        location="ch13-03-improving-our-io-project.md",
        number=[13, 3],
    ),
    Item(
        title="Comparing Performance: Loops vs. Iterators",
        location="ch13-04-performance.md",
        number=[13, 4],
    ),
    Item(
        title="More about Cargo and Crates.io",
        location="ch14-00-more-about-cargo.md",
        number=[14],
    ),
    Item(
        title="Customizing Builds with Release Profiles",
        location="ch14-01-release-profiles.md",
        number=[14, 1],
    ),
    Item(
        title="Publishing a Crate to Crates.io",
        location="ch14-02-publishing-to-crates-io.md",
        number=[14, 2],
    ),
    Item(
        title="Cargo Workspaces", location="ch14-03-cargo-workspaces.md", number=[14, 3]
    ),
    Item(
        title="Installing Binaries from Crates.io with cargo install",
        location="ch14-04-installing-binaries.md",
        number=[14, 4],
    ),
    Item(
        title="Extending Cargo with Custom Commands",
        location="ch14-05-extending-cargo.md",
        number=[14, 5],
    ),
    Item(title="Smart Pointers", location="ch15-00-smart-pointers.md", number=[15]),
    Item(
        title="Using Box<T> to Point to Data on the Heap",
        location="ch15-01-box.md",
        number=[15, 1],
    ),
    Item(
        title="Treating Smart Pointers Like Regular References with the Deref Trait",
        location="ch15-02-deref.md",
        number=[15, 2],
    ),
    Item(
        title="Running Code on Cleanup with the Drop Trait",
        location="ch15-03-drop.md",
        number=[15, 3],
    ),
    Item(
        title="Rc<T>, the Reference Counted Smart Pointer",
        location="ch15-04-rc.md",
        number=[15, 4],
    ),
    Item(
        title="RefCell<T> and the Interior Mutability Pattern",
        location="ch15-05-interior-mutability.md",
        number=[15, 5],
    ),
    Item(
        title="Reference Cycles Can Leak Memory",
        location="ch15-06-reference-cycles.md",
        number=[15, 6],
    ),
    Item(title="Fearless Concurrency", location="ch16-00-concurrency.md", number=[16]),
    Item(
        title="Using Threads to Run Code Simultaneously",
        location="ch16-01-threads.md",
        number=[16, 1],
    ),
    Item(
        title="Using Message Passing to Transfer Data Between Threads",
        location="ch16-02-message-passing.md",
        number=[16, 2],
    ),
    Item(
        title="Shared-State Concurrency",
        location="ch16-03-shared-state.md",
        number=[16, 3],
    ),
    Item(
        title="Extensible Concurrency with the Sync and Send Traits",
        location="ch16-04-extensible-concurrency-sync-and-send.md",
        number=[16, 4],
    ),
    Item(
        title="Object Oriented Programming Features of Rust",
        location="ch17-00-oop.md",
        number=[17],
    ),
    Item(
        title="Characteristics of Object-Oriented Languages",
        location="ch17-01-what-is-oo.md",
        number=[17, 1],
    ),
    Item(
        title="Using Trait Objects That Allow for Values of Different Types",
        location="ch17-02-trait-objects.md",
        number=[17, 2],
    ),
    Item(
        title="Implementing an Object-Oriented Design Pattern",
        location="ch17-03-oo-design-patterns.md",
        number=[17, 3],
    ),
    Item(title="Patterns and Matching", location="ch18-00-patterns.md", number=[18]),
    Item(
        title="All the Places Patterns Can Be Used",
        location="ch18-01-all-the-places-for-patterns.md",
        number=[18, 1],
    ),
    Item(
        title="Refutability: Whether a Pattern Might Fail to Match",
        location="ch18-02-refutability.md",
        number=[18, 2],
    ),
    Item(title="Pattern Syntax", location="ch18-03-pattern-syntax.md", number=[18, 3]),
    Item(
        title="Advanced Features", location="ch19-00-advanced-features.md", number=[19]
    ),
    Item(title="Unsafe Rust", location="ch19-01-unsafe-rust.md", number=[19, 1]),
    Item(
        title="Advanced Traits", location="ch19-03-advanced-traits.md", number=[19, 2]
    ),
    Item(title="Advanced Types", location="ch19-04-advanced-types.md", number=[19, 3]),
    Item(
        title="Advanced Functions and Closures",
        location="ch19-05-advanced-functions-and-closures.md",
        number=[19, 4],
    ),
    Item(title="Macros", location="ch19-06-macros.md", number=[19, 5]),
    Item(
        title="Final Project: Building a Multithreaded Web Server",
        location="ch20-00-final-project-a-web-server.md",
        number=[20],
    ),
    Item(
        title="Building a Single-Threaded Web Server",
        location="ch20-01-single-threaded.md",
        number=[20, 1],
    ),
    Item(
        title="Turning Our Single-Threaded Server into a Multithreaded Server",
        location="ch20-02-multithreaded.md",
        number=[20, 2],
    ),
    Item(
        title="Graceful Shutdown and Cleanup",
        location="ch20-03-graceful-shutdown-and-cleanup.md",
        number=[20, 3],
    ),
    Item(title="Appendix", location="appendix-00.md", number=[21]),
    Item(title="A - Keywords", location="appendix-01-keywords.md", number=[21, 1]),
    Item(
        title="B - Operators and Symbols",
        location="appendix-02-operators.md",
        number=[21, 2],
    ),
    Item(
        title="C - Derivable Traits",
        location="appendix-03-derivable-traits.md",
        number=[21, 3],
    ),
    Item(
        title="D - Useful Development Tools",
        location="appendix-04-useful-development-tools.md",
        number=[21, 4],
    ),
    Item(title="E - Editions", location="appendix-05-editions.md", number=[21, 5]),
    Item(
        title="F - Translations of the Book",
        location="appendix-06-translation.md",
        number=[21, 6],
    ),
    Item(
        title="G - How Rust is Made and “Nightly Rust”",
        location="appendix-07-nightly-rust.md",
        number=[21, 7],
    ),
]
CXX = [
    Item(title="Rust ❤️ C++", location="index.md", number=[1]),
    Item(title="Core concepts", location="concepts.md", number=[2]),
    Item(title="Tutorial", location="tutorial.md", number=[3]),
    Item(title="Other Rust–C++ interop tools", location="context.md", number=[4]),
    Item(
        title="Multi-language build system options", location="building.md", number=[5]
    ),
    Item(title="Cargo", location="build/cargo.md", number=[5, 1]),
    Item(title="Bazel", location="build/bazel.md", number=[5, 2]),
    Item(title="CMake", location="build/cmake.md", number=[5, 3]),
    Item(title="More...", location="build/other.md", number=[5, 4]),
    Item(title="Reference: the bridge module", location="reference.md", number=[6]),
    Item(title='extern "Rust"', location="extern-rust.md", number=[6, 1]),
    Item(title='extern "C++"', location="extern-c++.md", number=[6, 2]),
    Item(title="Shared types", location="shared.md", number=[6, 3]),
    Item(title="Attributes", location="attributes.md", number=[6, 4]),
    Item(title="Async functions", location="async.md", number=[6, 5]),
    Item(title="Error handling", location="binding/result.md", number=[6, 6]),
    Item(title="Reference: built-in bindings", location="bindings.md", number=[7]),
    Item(title="String — rust::String", location="binding/string.md", number=[7, 1]),
    Item(title="&str — rust::Str", location="binding/str.md", number=[7, 2]),
    Item(
        title="&[T], &mut [T] — rust::Slice<T>",
        location="binding/slice.md",
        number=[7, 3],
    ),
    Item(
        title="CxxString — std::string", location="binding/cxxstring.md", number=[7, 4]
    ),
    Item(title="Box<T> — rust::Box<T>", location="binding/box.md", number=[7, 5]),
    Item(
        title="UniquePtr<T> — std::unique_ptr<T>",
        location="binding/uniqueptr.md",
        number=[7, 6],
    ),
    Item(
        title="SharedPtr<T> — std::shared_ptr<T>",
        location="binding/sharedptr.md",
        number=[7, 7],
    ),
    Item(title="Vec<T> — rust::Vec<T>", location="binding/vec.md", number=[7, 8]),
    Item(
        title="CxxVector<T> — std::vector<T>",
        location="binding/cxxvector.md",
        number=[7, 9],
    ),
    Item(title="Function pointers", location="binding/fn.md", number=[7, 10]),
    Item(title="Result<T>", location="binding/result.md", number=[7, 11]),
]
MDBOOK = [
    Item(title="mdBook", location="README.md", number=[1]),
    Item(title="Command Line Tool", location="cli/README.md", number=[2]),
    Item(title="init", location="cli/init.md", number=[2, 1]),
    Item(title="build", location="cli/build.md", number=[2, 2]),
    Item(title="watch", location="cli/watch.md", number=[2, 3]),
    Item(title="serve", location="cli/serve.md", number=[2, 4]),
    Item(title="test", location="cli/test.md", number=[2, 5]),
    Item(title="clean", location="cli/clean.md", number=[2, 6]),
    Item(title="Format", location="format/README.md", number=[3]),
    Item(title="SUMMARY.md", location="format/summary.md", number=[3, 1]),
    Item(title="Draft chapter", location=None, number=[3, 1, 1]),
    Item(title="Configuration", location="format/config.md", number=[3, 2]),
    Item(title="Theme", location="format/theme/README.md", number=[3, 3]),
    Item(title="index.hbs", location="format/theme/index-hbs.md", number=[3, 3, 1]),
    Item(
        title="Syntax highlighting",
        location="format/theme/syntax-highlighting.md",
        number=[3, 3, 2],
    ),
    Item(title="Editor", location="format/theme/editor.md", number=[3, 3, 3]),
    Item(title="MathJax Support", location="format/mathjax.md", number=[3, 4]),
    Item(title="mdBook-specific features", location="format/mdbook.md", number=[3, 5]),
    Item(
        title="Continuous Integration", location="continuous-integration.md", number=[4]
    ),
    Item(title="For Developers", location="for_developers/README.md", number=[5]),
    Item(
        title="Preprocessors", location="for_developers/preprocessors.md", number=[5, 1]
    ),
    Item(
        title="Alternative Backends",
        location="for_developers/backends.md",
        number=[5, 2],
    ),
    Item(title="Contributors", location="misc/contributors.md", number=None),
]
RCDG = [
    Item(title="About this guide", location="./about-this-guide.md", number=None),
    Item(title="Getting Started", location="./getting-started.md", number=None),
    Item(
        title="How to Build and Run the Compiler",
        location="./building/how-to-build-and-run.md",
        number=[1],
    ),
    Item(title="Prerequisites", location="./building/prerequisites.md", number=[1, 1]),
    Item(
        title="Suggested Workflows", location="./building/suggested.md", number=[1, 2]
    ),
    Item(
        title="Distribution artifacts",
        location="./building/build-install-distribution-artifacts.md",
        number=[1, 3],
    ),
    Item(
        title="Documenting Compiler",
        location="./building/compiler-documenting.md",
        number=[1, 4],
    ),
    Item(title="Rustdoc", location="./rustdoc.md", number=[1, 5]),
    Item(title="ctags", location="./building/ctags.md", number=[1, 6]),
    Item(
        title="Adding a new target", location="./building/new-target.md", number=[1, 7]
    ),
    Item(
        title="The compiler testing framework", location="./tests/intro.md", number=[2]
    ),
    Item(title="Running tests", location="./tests/running.md", number=[2, 1]),
    Item(title="Adding new tests", location="./tests/adding.md", number=[2, 2]),
    Item(
        title="Using compiletest commands to control test execution",
        location="./compiletest.md",
        number=[2, 3],
    ),
    Item(
        title="Debugging the Compiler", location="./compiler-debugging.md", number=[3]
    ),
    Item(title="Profiling the compiler", location="./profiling.md", number=[4]),
    Item(
        title="with the linux perf tool",
        location="./profiling/with_perf.md",
        number=[4, 1],
    ),
    Item(title="crates.io Dependencies", location="./crates-io.md", number=[5]),
    Item(title="Introduction", location="./contributing.md", number=[6]),
    Item(title="About the compiler team", location="./compiler-team.md", number=[7]),
    Item(title="Using Git", location="./git.md", number=[8]),
    Item(title="Mastering @rustbot", location="./rustbot.md", number=[9]),
    Item(
        title="Walkthrough: a typical contribution",
        location="./walkthrough.md",
        number=[10],
    ),
    Item(title="Bug Fix Procedure", location="./bug-fix-procedure.md", number=[11]),
    Item(
        title="Implementing new features",
        location="./implementing_new_features.md",
        number=[12],
    ),
    Item(title="Stability attributes", location="./stability.md", number=[13]),
    Item(
        title="Stabilizing Features", location="./stabilization_guide.md", number=[14]
    ),
    Item(title="Feature Gates", location="./feature-gates.md", number=[15]),
    Item(title="Coding conventions", location="./conventions.md", number=[16]),
    Item(
        title="Notification groups",
        location="notification-groups/about.md",
        number=[17],
    ),
    Item(title="ARM", location="notification-groups/arm.md", number=[17, 1]),
    Item(
        title="Cleanup Crew",
        location="notification-groups/cleanup-crew.md",
        number=[17, 2],
    ),
    Item(title="LLVM", location="notification-groups/llvm.md", number=[17, 3]),
    Item(title="RISC-V", location="notification-groups/risc-v.md", number=[17, 4]),
    Item(title="Windows", location="notification-groups/windows.md", number=[17, 5]),
    Item(title="Licenses", location="./licenses.md", number=[18]),
    Item(title="Prologue", location="./part-2-intro.md", number=[19]),
    Item(title="Overview of the Compiler", location="./overview.md", number=[20]),
    Item(title="The compiler source code", location="./compiler-src.md", number=[21]),
    Item(title="Bootstrapping", location="./building/bootstrapping.md", number=[22]),
    Item(
        title="Queries: demand-driven compilation", location="./query.md", number=[23]
    ),
    Item(
        title="The Query Evaluation Model in Detail",
        location="./queries/query-evaluation-model-in-detail.md",
        number=[23, 1],
    ),
    Item(
        title="Incremental compilation",
        location="./queries/incremental-compilation.md",
        number=[23, 2],
    ),
    Item(
        title="Incremental compilation In Detail",
        location="./queries/incremental-compilation-in-detail.md",
        number=[23, 3],
    ),
    Item(
        title="Debugging and Testing",
        location="./incrcomp-debugging.md",
        number=[23, 4],
    ),
    Item(title="Profiling Queries", location="./queries/profiling.md", number=[23, 5]),
    Item(title="Salsa", location="./salsa.md", number=[23, 6]),
    Item(title="Memory Management in Rustc", location="./memory.md", number=[24]),
    Item(title="Serialization in Rustc", location="./serialization.md", number=[25]),
    Item(title="Parallel Compilation", location="./parallel-rustc.md", number=[26]),
    Item(title="Rustdoc", location="./rustdoc-internals.md", number=[27]),
    Item(title="Prologue", location="./part-3-intro.md", number=[28]),
    Item(title="Command-line arguments", location="./cli.md", number=[29]),
    Item(
        title="The Rustc Driver and Interface",
        location="./rustc-driver.md",
        number=[30],
    ),
    Item(
        title="Ex: Type checking through rustc_interface",
        location="./rustc-driver-interacting-with-the-ast.md",
        number=[30, 1],
    ),
    Item(
        title="Ex: Getting diagnostics through rustc_interface",
        location="./rustc-driver-getting-diagnostics.md",
        number=[30, 2],
    ),
    Item(title="Syntax and the AST", location="./syntax-intro.md", number=[31]),
    Item(title="Lexing and Parsing", location="./the-parser.md", number=[31, 1]),
    Item(title="Macro expansion", location="./macro-expansion.md", number=[31, 2]),
    Item(title="Name resolution", location="./name-resolution.md", number=[31, 3]),
    Item(
        title="#[test] Implementation",
        location="./test-implementation.md",
        number=[31, 4],
    ),
    Item(
        title="Panic Implementation",
        location="./panic-implementation.md",
        number=[31, 5],
    ),
    Item(title="AST Validation", location="./ast-validation.md", number=[31, 6]),
    Item(
        title="Feature Gate Checking", location="./feature-gate-ck.md", number=[31, 7]
    ),
    Item(title="The HIR (High-level IR)", location="./hir.md", number=[32]),
    Item(title="Lowering AST to HIR", location="./lowering.md", number=[32, 1]),
    Item(title="Debugging", location="./hir-debugging.md", number=[32, 2]),
    Item(title="The MIR (Mid-level IR)", location="./mir/index.md", number=[33]),
    Item(
        title="THIR and MIR construction",
        location="./mir/construction.md",
        number=[33, 1],
    ),
    Item(
        title="MIR visitor and traversal", location="./mir/visitor.md", number=[33, 2]
    ),
    Item(
        title="MIR passes: getting the MIR for a function",
        location="./mir/passes.md",
        number=[33, 3],
    ),
    Item(title="Identifiers in the Compiler", location="./identifiers.md", number=[34]),
    Item(title="Closure expansion", location="./closure.md", number=[35]),
    Item(title="Prologue", location="./part-4-intro.md", number=[36]),
    Item(title="The ty module: representing types", location="./ty.md", number=[37]),
    Item(title="Generics and substitutions", location="./generics.md", number=[37, 1]),
    Item(title="TypeFolder and TypeFoldable", location="./ty-fold.md", number=[37, 2]),
    Item(title="Generic arguments", location="./generic_arguments.md", number=[37, 3]),
    Item(title="Type inference", location="./type-inference.md", number=[38]),
    Item(title="Trait solving", location="./traits/resolution.md", number=[39]),
    Item(
        title="Early and Late Bound Parameters",
        location="./early-late-bound.md",
        number=[39, 1],
    ),
    Item(
        title="Higher-ranked trait bounds", location="./traits/hrtb.md", number=[39, 2]
    ),
    Item(title="Caching subtleties", location="./traits/caching.md", number=[39, 3]),
    Item(title="Specialization", location="./traits/specialization.md", number=[39, 4]),
    Item(
        title="Chalk-based trait solving", location="./traits/chalk.md", number=[39, 5]
    ),
    Item(
        title="Lowering to logic",
        location="./traits/lowering-to-logic.md",
        number=[39, 5, 1],
    ),
    Item(
        title="Goals and clauses",
        location="./traits/goals-and-clauses.md",
        number=[39, 5, 2],
    ),
    Item(
        title="Canonical queries",
        location="./traits/canonical-queries.md",
        number=[39, 5, 3],
    ),
    Item(
        title="Lowering module in rustc",
        location="./traits/lowering-module.md",
        number=[39, 5, 4],
    ),
    Item(title="Type checking", location="./type-checking.md", number=[40]),
    Item(title="Method Lookup", location="./method-lookup.md", number=[40, 1]),
    Item(title="Variance", location="./variance.md", number=[40, 2]),
    Item(
        title="Opaque Types",
        location="./opaque-types-type-alias-impl-trait.md",
        number=[40, 3],
    ),
    Item(
        title="Pattern and Exhaustiveness Checking",
        location="./pat-exhaustive-checking.md",
        number=[41],
    ),
    Item(title="MIR dataflow", location="./mir/dataflow.md", number=[42]),
    Item(title="The borrow checker", location="./borrow_check.md", number=[43]),
    Item(
        title="Tracking moves and initialization",
        location="./borrow_check/moves_and_initialization.md",
        number=[43, 1],
    ),
    Item(
        title="Move paths",
        location="./borrow_check/moves_and_initialization/move_paths.md",
        number=[43, 1, 1],
    ),
    Item(
        title="MIR type checker",
        location="./borrow_check/type_check.md",
        number=[43, 2],
    ),
    Item(
        title="Region inference",
        location="./borrow_check/region_inference.md",
        number=[43, 3],
    ),
    Item(
        title="Constraint propagation",
        location="./borrow_check/region_inference/constraint_propagation.md",
        number=[43, 3, 1],
    ),
    Item(
        title="Lifetime parameters",
        location="./borrow_check/region_inference/lifetime_parameters.md",
        number=[43, 3, 2],
    ),
    Item(
        title="Member constraints",
        location="./borrow_check/region_inference/member_constraints.md",
        number=[43, 3, 3],
    ),
    Item(
        title="Placeholders and universes",
        location="./borrow_check/region_inference/placeholders_and_universes.md",
        number=[43, 3, 4],
    ),
    Item(
        title="Closure constraints",
        location="./borrow_check/region_inference/closure_constraints.md",
        number=[43, 3, 5],
    ),
    Item(
        title="Error reporting",
        location="./borrow_check/region_inference/error_reporting.md",
        number=[43, 3, 6],
    ),
    Item(
        title="Two-phase-borrows",
        location="./borrow_check/two_phase_borrows.md",
        number=[43, 4],
    ),
    Item(title="Parameter Environments", location="./param_env.md", number=[44]),
    Item(title="Errors and Lints", location="diagnostics.md", number=[45]),
    Item(
        title="Creating Errors With SessionDiagnostic",
        location="./diagnostics/sessiondiagnostic.md",
        number=[45, 1],
    ),
    Item(title="LintStore", location="./diagnostics/lintstore.md", number=[45, 2]),
    Item(
        title="Diagnostic Codes",
        location="./diagnostics/diagnostic-codes.md",
        number=[45, 3],
    ),
    Item(title="Prologue", location="./part-5-intro.md", number=[46]),
    Item(title="MIR optimizations", location="./mir/optimizations.md", number=[47]),
    Item(title="Debugging", location="./mir/debugging.md", number=[48]),
    Item(title="Constant evaluation", location="./const-eval.md", number=[49]),
    Item(title="miri const evaluator", location="./miri.md", number=[49, 1]),
    Item(title="Monomorphization", location="./backend/monomorph.md", number=[50]),
    Item(title="Lowering MIR", location="./backend/lowering-mir.md", number=[51]),
    Item(title="Code Generation", location="./backend/codegen.md", number=[52]),
    Item(title="Updating LLVM", location="./backend/updating-llvm.md", number=[52, 1]),
    Item(title="Debugging LLVM", location="./backend/debugging.md", number=[52, 2]),
    Item(
        title="Backend Agnostic Codegen",
        location="./backend/backend-agnostic.md",
        number=[52, 3],
    ),
    Item(
        title="Implicit Caller Location",
        location="./backend/implicit-caller-location.md",
        number=[52, 4],
    ),
    Item(
        title="Profile-guided Optimization",
        location="./profile-guided-optimization.md",
        number=[53],
    ),
    Item(
        title="LLVM Source-Based Code Coverage",
        location="./llvm-coverage-instrumentation.md",
        number=[54],
    ),
    Item(title="Sanitizers Support", location="./sanitizers.md", number=[55]),
    Item(
        title="Debugging Support in the Rust Compiler",
        location="./debugging-support-in-rustc.md",
        number=[56],
    ),
    Item(
        title="Appendix A: Background topics",
        location="./appendix/background.md",
        number=None,
    ),
    Item(title="Appendix B: Glossary", location="./appendix/glossary.md", number=None),
    Item(
        title="Appendix C: Code Index", location="./appendix/code-index.md", number=None
    ),
    Item(
        title="Appendix D: Compiler Lecture Series",
        location="./appendix/compiler-lecture.md",
        number=None,
    ),
    Item(
        title="Appendix E: Bibliography",
        location="./appendix/bibliography.md",
        number=None,
    ),
    Item(title="Appendix Z: HumorRust", location="./appendix/humorust.md", number=None),
]


class ParseTests(unittest.TestCase):
    def do_test(self, name, items):
        with open(path.join(TEST_DATA_DIR, f"{name}-summary.json")) as f:
            self.assertEqual(list(summary.parse(json.load(f))), items)

    def test_trpl(self):
        self.do_test("book", TRPL)

    def test_cxx(self):
        self.do_test("cxx", CXX)

    def test_mdbook(self):
        self.do_test("mdbook", MDBOOK)

    def test_dev_guide(self):
        self.do_test("rustc", RCDG)


if __name__ == "__main__":
    unittest.main()
